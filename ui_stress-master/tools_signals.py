import neurokit2 as nk
import matplotlib.pyplot as plt
import scipy.io as sio
from mat4py import loadmat
import numpy as np
import os
import pandas as pd
from scipy import signal
from scipy.stats import entropy
import antropy as ent


class ManagerTools():

    def __init__(self):
        pass

    def SCR_features(self, SCR_feat):
        features1_df = pd.DataFrame(SCR_feat, index=['SCR_mean_Amplitude', 'SCR_mean_Height', 'SCR_mean_Recovery',
                                                     'SCR_mean_RiseTime'])
        SCR_features1_df = features1_df.mean(axis=1)
        SCR_features1_df['SCR_number'] = 100 * features1_df.iloc[0].count()
        SCR_features1_df = pd.DataFrame(SCR_features1_df).transpose().fillna(0)

        return (SCR_features1_df)

    def signal_features(self, signals1):

        signals1 = signals1[['EDA_Raw', 'EDA_Clean', 'EDA_Tonic', 'EDA_Phasic']]

        # Define signal for EMD
        ss = signals1[['EDA_Clean']].to_numpy().flatten(order='C')

        # Execute EMD on signal

        # ********************** Add IMF1 to signals ********************
        # signals1 = pd.concat([signals1,IMFs])
        signals1 = pd.concat([signals1])

        # ********************** Signal features ************************
        mins = signals1.min(axis=0)
        mins = pd.DataFrame(mins).transpose().fillna(0)

        means = signals1.mean(axis=0)
        means = pd.DataFrame(means).transpose().fillna(0)

        medians = signals1.median(axis=0)
        medians = pd.DataFrame(medians).transpose().fillna(0)

        maxs = signals1.max(axis=0)
        maxs = pd.DataFrame(maxs).transpose().fillna(0)

        variances = signals1.var(axis=0)
        variances = pd.DataFrame(variances).transpose().fillna(0)

        stds = signals1.std(axis=0)
        stds = pd.DataFrame(stds).transpose().fillna(0)

        energy = pd.Series(np.power(signals1, 2).sum())
        energy = pd.DataFrame(energy).transpose()

        RMSE = (energy / len(signals1)) ** (1 / 2)

        kurts = signals1.kurtosis(axis=0)
        kurts = pd.DataFrame(kurts).transpose().fillna(0)
        print(len(kurts))

        skews = signals1.skew(axis=0)
        skews = pd.DataFrame(skews).transpose().fillna(0)

        lz_complex = pd.Series(ent.lziv_complexity(ss, normalize=True))
        # sample_ent = pd.Series(ent.sample_entropy(ss))
        svd_end = pd.Series(ent.svd_entropy(ss, normalize=True))

        entropys = entropy(signals1)
        entropys = pd.DataFrame(entropys).transpose().fillna(0)

        spec_ent = ent.spectral_entropy(signals1, sf=100, method='welch', normalize=True, axis=0)
        spec_ent = pd.DataFrame(spec_ent).transpose().fillna(0)

        # N_imfs = len(IMF)

        feat = ['RMSE', 'energy', 'min', 'mean', 'median', 'max', 'var', 'std', 'kurt', 'skew', 'LZ_complexity',
                'svd_ent', 'entropy', 'spectral_entropy']

        signals1_features = pd.concat(
            [RMSE, energy, mins, means, medians, maxs, variances, stds, kurts, skews, lz_complex, svd_end, entropys,
             spec_ent], keys=feat, axis=1)

        signals1_features.columns = ['RMSE_raw', 'RMSE_clean', 'RMSE_tonic', 'RMSE_phasic',
                                     'energy_raw', 'energy_clean', 'energy_tonic', 'energy_phasic',
                                     'min_raw', 'min_clean', 'min_tonic', 'min_phasic',
                                     'mean_raw', 'mean_clean', 'mean_tonic', 'mean_phasic',
                                     'median_raw', 'median_clean', 'median_tonic', 'median_phasic',
                                     'max_raw', 'max_clean', 'max_tonic', 'max_phasic',
                                     'var_raw', 'var_clean', 'var_tonic', 'var_phasic',
                                     'std_raw', 'std_clean', 'std_tonic', 'std_phasic',
                                     'kurt_raw', 'kurt_clean', 'kurt_tonic', 'kurt_phasic',
                                     'skew_raw', 'skew_clean', 'skew_tonic', 'skew_phasic',
                                     'lz_complex', 'svd_ent',
                                     'entropy_raw', 'entropy_clean', 'entropy_tonic', 'entropy_phasic',
                                     'spec_entropy_raw', 'spec_entropy_clean', 'spec_entropy_tonic',
                                     'spec_entropy_phasic']

        # signals1_features["N_IMFs"] = N_imfs

        # print(signals1_features)

        return (signals1_features)

    def EDA_processing(self, eda_signal3, timestamps, fs):
        [eda_signal3, timestamps_resampled] = signal.resample(eda_signal3, int(len(eda_signal3) / 16), timestamps)
        eda_signal3_filt = nk.signal_filter(eda_signal3, sampling_rate=fs, highcut=0.05, method='butterworth',
                                            order=4)

        # Process the raw EDA signal
        # signals, info = nk.eda_process(eda_signal,method='neurokit')
        signals3, info3 = nk.eda_process(eda_signal3_filt)

        # Extract clean EDA and SCR features ***************************************

        cleaned3 = signals3["EDA_Clean"]
        features3 = [info3["SCR_Onsets"], info3["SCR_Peaks"], info3["SCR_Recovery"]]

        # Select SCR features according to conditions ***************************************
        #     min_SCRamplitude = 0.01

        for i in range(1, len(info3["SCR_Onsets"])):
            diff = info3["SCR_Onsets"][i] - info3["SCR_Peaks"][i - 1]
            if diff < 1000:
                if info3["SCR_Height"][i] < info3["SCR_Height"][i - 1]:

                    info3["SCR_Amplitude"][i - 1] = info3["SCR_Amplitude"][i] + info3["SCR_Amplitude"][i - 1]

                    info3["SCR_Onsets"][i] = 0
                    info3["SCR_Recovery"][i] = 0
                    info3["SCR_Peaks"][i] = 0
                    info3["SCR_RiseTime"][i] = 0
                    info3["SCR_Height"][i] = 0

                else:
                    info3["SCR_Amplitude"][i] = info3["SCR_Amplitude"][i] + info3["SCR_Amplitude"][i - 1]

                    info3["SCR_Onsets"][i - 1] = 0
                    info3["SCR_Recovery"][i - 1] = 0
                    info3["SCR_Peaks"][i - 1] = 0
                    info3["SCR_RiseTime"][i - 1] = 0
                    info3["SCR_Height"][i - 1] = 0

        #             info3["SCR_Amplitude"][i-1] = max(info3["SCR_Amplitude"][i], info3["SCR_Amplitude"][i-1])

        #             amp_cond3 = np.where(info3["SCR_Onsets"][i] < diff_min, 0, info3["SCR_Amplitude"])

        amp_cond3 = np.where(info3["SCR_Amplitude"] < 0.05, 0, info3["SCR_Amplitude"])

        info3["SCR_Onsets"] = info3["SCR_Onsets"][amp_cond3 > 0]
        info3["SCR_Recovery"] = info3["SCR_Recovery"][amp_cond3 > 0]
        info3["SCR_Peaks"] = info3["SCR_Peaks"][amp_cond3 > 0]
        info3["SCR_RiseTime"] = info3["SCR_RiseTime"][amp_cond3 > 0]

        info3["SCR_Amplitude"] = info3["SCR_Amplitude"][amp_cond3 > 0]
        info3["SCR_Height"] = info3["SCR_Height"][amp_cond3 > 0]

        SCR_features3 = [info3["SCR_Amplitude"], info3["SCR_Height"], info3["SCR_Recovery"], info3["SCR_RiseTime"]]

        features3 = [info3["SCR_Onsets"], info3["SCR_Peaks"], info3["SCR_Recovery"]]

        # Visualize SCR features in cleaned EDA signal ***************************************
        plot = nk.events_plot(features3, cleaned3, color=['red', 'blue', 'orange'])

        # Filter phasic and tonic components ***************************************
        data3 = nk.eda_phasic(nk.standardize(eda_signal3_filt, robust=True))
        # eda_phasic = data["EDA_Phasic"].values

        # Plot EDA signal ***************************************

        return (signals3, SCR_features3, timestamps_resampled)

