# UI / DB Systemè pour le traitement des signaux ECG,EMG,EDA 

Logiciel développé sur Python3.8.10

## Modules utilisés

1. PyQt5   5.15.4
1. pyqt5-plugins 5.15.4.2.2
1. PyQt5-Qt5 5.15.2
1. PyQt5-sip 12.9.0
1. pyqt5-tools 5.15.4.3.2
1. PyQt5Designer 5.14.1
1. pyqtgraph 0.12.2
1. qt5-applications 5.15.2.2.2
1. qt5-tools 5.15.2.1.2
