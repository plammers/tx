function ecg2cwtscg(ecgdata,cwtfb,ecgtype,j)

colormap = jet(128);

if ecgtype == 'relax'
    folderpath = strcat('images/relax/');
    cfs = abs(cwtfb.wt(ecgdata));
    im = ind2rgb(im2uint8(rescale(cfs)), colormap);
    filename = strcat(folderpath,sprintf('%d.jpg', j));
    imwrite(imresize(im,[227 227]), filename);

elseif ecgtype == 'stress'
    folderpath = strcat('images/stress/');
    cfs = abs(cwtfb.wt(ecgdata));
    im = ind2rgb(im2uint8(rescale(cfs)), colormap);
    filename = 2(folderpath,sprintf('%d.jpg', j));
    imwrite(imresize(im,[227 227]), filename);

end
