%Training and Validation using Alexnet
DatasetPath = "/images";
%Reading Images from image Database Folder
images = imageDatastore(DatasetPath, 'IncludeSubfolders',true,'LabelSource','foldernames');
%Distributing Images in the set of Training and Testing
numTrainFiles = 10000;
[TrainImages,TestImages] = splitEachLabel(images, numTrainFiles, 'randomize');
net = alexnet; % importing pretrained Alexnet (Requires support package)
layersTransfer = net.Layers(1:end-3); %Preserving all layers except last three

numClasses = 2; % Number of output classes : relax and stress
%Defining layers of AlexNet
layers = [layersTransfer
fullyConnectedLayer(numClasses,'WeightLearnRateFactor',20,'BiasLearnRateFactor',20)
softmaxLayer
classificationLayer];
%Training options
options = trainingOptions('sgdm', 'MiniBatchSize', 20, 'MaxEpochs', 8, 'InitialLearnRate', 1e-4,'Shuffle', 'every-epoch','ValidationData', TestImages, 'ValidationFrequency', 10, 'Verbose', false, 'Plots', 'training-progress');
%Training the AlexNet
netTransfer  = (TrainImages, layers, options);

%save the deep neural network
outputDir = "";
outputFile = fullfile(outputDir, "netTransfer.mat");
save(outputFile, "netTransfer");

%classifying Images
YPred= classify(netTransfer, TestImages);
YValidation = TestImages.Labels;
accuracy = sum(YPred == YValidation)/numel(YValidation)

%Plotting Confusion Matrix 
plotconfusion(YValidation, YPred)
%save the plot
saveas(gcf,'plotconfusion.pdf')
