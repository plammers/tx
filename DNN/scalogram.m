%create folder
mkdir('images');
mkdir('images_relax');
mkdir('images_stress');
signal_length = 2000;
%create the filter for CWT with amor wavelet and 12 filters per octave
fb = cwtfilterbank('SignalLength',signal_length,'Wavelet','amor','VoicesPerOctave',12);
%ECG type
ecgtype={'relax','stress'};

%load the relax data
projectdir = 'framed_data/stress/';
dinfo = dir(fullfile(projectdir));
dinfo([dinfo.isdir]) = []; 
nfiles = length(dinfo);

for j = 1 : nfiles
  disp(j)
  filename = fullfile(projectdir, dinfo(j).name);
  ECG = load(filename);
  %function to convert into image
  if length(ECG.ECG) == signal_length
    ecg2cwtscg(ECG.ECG, fb, ecgtype{1}, j);
  end
end

%load the relax data
projectdir = 'framed_data/relax/';
dinfo = dir(fullfile(projectdir));
dinfo([dinfo.isdir]) = []; 
nfiles = length(dinfo);

for j = 1 : nfiles
  disp(j)
  filename = fullfile(projectdir, dinfo(j).name);
  ECG = load(filename);
  %function to convert into image
  if length(ECG.ECG) == signal_length
    ecg2cwtscg(ECG.ECG, fb, ecgtype{2}, j);
  end
end


