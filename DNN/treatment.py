from __future__ import annotations
from scipy.signal import butter, sosfilt
from scipy.io import savemat
from numpy import ndarray
from typing import Tuple, List, Union
import data_import as io
import numpy as np
import pywt
from ecgdetectors import Detectors


class Signal:
    """Class that represents a signal, on which we can apply different analysis
    and treatments methods.
    """

    data:ndarray
    """Signal data as an array."""
    original_data:ndarray
    """Untreated data as an aaray."""
    time:ndarray
    """Signal timestamp as an array (same size as data)."""
    frequency_sample:int = 512
    """Signal frequency sample in Hz."""
    r_peaks:Union[ndarray, None]
    """Array containing the indexes of the R peaks of the signal."""

    def __init__(self, file:str=None, data:np.ndarray=None, r_peaks_detection:bool=True) -> Signal:
        """Build a Signal from a `.mat` file or from arrays.

        Args:
        -----
            file (str): Path to `.mat` file. Defaults to None.
            data (np.ndarray): Singal's data in a (2 x n) array of form [data, time].
            r_peaks_detection (bool): Do R peak detection on the signal. Defaults to True.
        """
        # Loading data from .mat file and assiging it to class' attributes.
        if file is not None:
            self.time, self.data = io.import_ecg(file)
        elif data is not None:
            self.data = data[:, 0]
            self.time = data[:, 1]
        else:
            raise ValueError("You must give a filename or some data to build a signal.")

        self.original_data = np.copy(self.data)

        # Detecting R peaks
        if r_peaks_detection is True:
            detector = Detectors(Signal.frequency_sample)
            self.r_peaks = detector.two_average_detector(self.original_data)
        else:
            self.r_peaks = None

    @property
    def r_peaks_times(self) -> ndarray:
        """Get the the timestamps of each R peaks.

        Returns:
        --------
            ndarray: Array containing the timestamps of each R peak.
        """
        return self.time[self.r_peaks]

    @property
    def r_peaks_values(self) -> ndarray:
        """Get the the values of each R peaks.

        Returns:
        --------
            ndarray: Array containing the values of each R peak.
        """
        return self.data[self.r_peaks]

    def apply_bandpass_filter(self, frequencies:Tuple[int, int], order:int = 2) -> None:
        """Apply butterworth (or bandpass) filter on the singal's data.

        Args:
        -----
            frequencies (Tuple[int, int]): Tuple containing the range of frequencies
            to keep.
            order (int, optional): Order of the filter. Defaults to 2.
        """
        # Creating filter
        filter = butter(
            order,
            frequencies,
            'bandpass',
            fs=Signal.frequency_sample,
            output='sos'
        )
        # Applying filter to singal's data
        self.data = sosfilt(filter, self.data)

    def apply_bandstop_filter(self, notch_frquency:float, bandwidth:float) -> None:
        """Apply a bandstop (or notch) filter on the signal's data.

        Args:
        -----
            notch_frquency (float): Frequency of the notch in Hz.
            bandwidth (float): Width of the notch in Hz in which singal's gain
            will be smaller than 0.7
        """
        frequencies = (notch_frquency-bandwidth/2, notch_frquency+bandwidth/2)
        # Creating filter
        filter = butter(
            3,
            frequencies,
            'bandstop',
            fs=Signal.frequency_sample,
            output='sos'
        )
        self.data = sosfilt(filter, self.data)

    def dwt_thresholding(self, hard:bool=True, static_thresholh:float = None) -> None:
        """Apply Discrete Wavelet Transform on the signal.

        Args:
        -----
            hard (bool, optional): Use hard thresholding is set to true, soft otherwise.
            Defaults to True.
            static_thresholh (float, optional): Static threshold to use. If None, will
            compute dynamic threshold. Defaults to None.
        """
        # Getting coefficient from signal's data discret wavelet transform
        coeffs:List[ndarray] = pywt.wavedec(self.data, 'db5', level=3)

        # Getting threshholding method
        mode = 'hard' if hard is True else 'soft'

        # If no static threshold was given, apply dynamic threshold
        if static_thresholh is None:
            a = pywt.threshold(
                coeffs[0],
                np.std(coeffs[1]) * np.sqrt(2*np.log(coeffs[0].size)),
                mode
            )
            b = pywt.threshold(
                coeffs[1],
                np.std(coeffs[1]) * np.sqrt(2*np.log(coeffs[1].size)),
                mode
            )
            c = pywt.threshold(
                coeffs[2],
                np.std(coeffs[1]) * np.sqrt(2*np.log(coeffs[2].size)),
                mode
            )
            d = pywt.threshold(
                coeffs[3],
                np.std(coeffs[1]) * np.sqrt(2*np.log(coeffs[3].size)),
                mode
            )
        # Otherwise apply static thresholding
        else:
            a = pywt.threshold(coeffs[0], static_thresholh, mode)
            b = pywt.threshold(coeffs[1], static_thresholh, mode)
            c = pywt.threshold(coeffs[2], static_thresholh, mode)
            d = pywt.threshold(coeffs[3], static_thresholh, mode)

        # Build back the signal with thresholded coefficients
        self.data = pywt.waverec([a, b, c, d], 'db5')

    def hrv_signal(self) -> Tuple[np.ndarray, np.ndarray]:
        """Get the HRV singal of the signal.

        Returns:
        --------
            Tuple[np.ndarray, np.ndarray]: Tuple containing HRV signal and timestamps.
        """
        rr_intervals = self.r_peaks_times[1:] - self.r_peaks_times[:-1]
        timestamps = self.r_peaks_times[:-1]

        return (rr_intervals, timestamps)

    def export_to_mat(self, filename:str) -> None:
        """Saves Signal's data into a .mat file.

        Args:
        -----
            filename (str): Path to save file. (Should be ending with `.mat`)
        """
        mat_file = dict()
        mat_file['ECG2_TimestampSync_Unix_CAL'] = self.time
        mat_file['ECG'] = self.data
        savemat(filename, mat_file)

    def export_to_csv(self, filename:str, delimiter:str=',') -> None:
        """Saves Signal's data into a .mat file.

        Args:
        -----
            filename (str): Path to save file. (Should be ending with `.csv`)
            delimiter (str, optional): CSV values delimiter. Defaults to ','.
        """
        data = self.data.tolist()
        data.insert(0, 'ECG')

        time = self.time.tolist()
        time.insert(0, 'ECG2_TimestampSync_Unix_CAL')

        np.savetxt(filename, [p for p in zip(data, time)], delimiter=delimiter, fmt='%s')
