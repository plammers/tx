# Library imports
import os
import matplotlib.pyplot as plt
import numpy as np
import pyhrv

# Local imports
import treatment

# filename = os.path.join('data', '8_Session1_ECG2_Calibrated_PC.mat')
filename = os.path.join('framed_data', 'relax', 'relax_1.mat')
signal = treatment.Signal(filename)

# plt.plot(signal.time, signal.data, 'g', label='Raw singal')

# signal.apply_bandstop_filter(80, 30)
# signal.apply_bandstop_filter(50, 30)

# plt.plot(signal.time, signal.data, 'r', label='Notch filtered')

# signal.apply_bandpass_filter((3, 100), 3)

# r_peaks_values = signal.data[r_peaks]

# pyhrv.hrv(signal=signal.data, sampling_rate=signal.frequency_sample, plot_ecg=False, plot_tachogram=False, show=False)

# plt.plot(signal.time[1000:2000], signal.data[1000:2000], label='Butterworth filtered')
# plt.plot(signal.time, signal.data, label='Butterworth filtered')
# plt.plot(signal.r_peaks_times, signal.r_peaks_values, 'ro')

# signal.dwt_thresholding(True, 0.03)
# signal.dwt_thresholding(True, 0.15)

# signal.export_to_csv('Test.csv')

# hrv = signal.hrv_signal()
# plt.step(hrv[1], hrv[0])
plt.plot(signal.data, 'b', label='DWT')

# plt.legend()
plt.show()