import treatment
import os
import matplotlib.pyplot as plt
import numpy as np

filename = os.path.join('data', '8_Session1_ECG2_Calibrated_PC.mat')
signal = treatment.Signal(filename)

stress_index = 0
relax_index = 0

for filename in os.listdir('data'):
    # Determine wether it is a relax or stress recording sessions
    if 'Session1' in filename:
        type = 'relax'
    elif 'Session2' in filename:
        type = 'stress'
    else:
        continue

    # Printing the filename to keep track of the progress
    print(filename)

    # Building signal from file
    signal = treatment.Signal(os.path.join('data', filename))

    # Filtering signal
    signal.apply_bandstop_filter(50, 30)
    signal.apply_bandpass_filter((3, 100), 3)

    # Cutting the signal in subframes
    for i in range(1000, signal.data.size, 2000):
        data = signal.data[i:i+2000].T
        time = signal.time[i:i+2000].T

        sub_signal = treatment.Signal(data=np.stack((data, time), axis=-1), r_peaks_detection=False)

        # Saving signal to new mat file
        if type == 'relax':
            sub_signal.export_to_mat(f"framed_data/relax/relax_{relax_index}.mat")
            relax_index += 1
        elif type == 'stress':
            sub_signal.export_to_mat(f"framed_data/stress/stress_{stress_index}.mat")
            stress_index += 1
