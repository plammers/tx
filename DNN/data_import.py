from scipy.io import loadmat
from typing import Tuple
from numpy import ndarray

def import_ecg(filename:str) -> Tuple[ndarray, ndarray]:
    """Opens a .mat file and returns ECG data contained in it.

    Args:
    -----
        filename (str): Relative path to the .mat file.

    Returns:
    --------
        Tuple[ndarray, ndarray]: Tuple containing arrays of time and ECG data.
    """
    # Loading .mat file
    matfile = loadmat(filename)

    if 'ECG1' in filename:
        number = 1
    else:
        number = 2

    # Extracting time data
    time = (ndarray.flatten(matfile[f'ECG{number}_TimestampSync_Unix_CAL']) - matfile[f'ECG{number}_TimestampSync_Unix_CAL'][0]) / 1000
    # Extracting ECG data
    try:
        ecg = ndarray.flatten(matfile[f'ECG{number}_ECG_LA_RA_24BIT_CAL'])
    except KeyError:
        ecg = ndarray.flatten(matfile['ECG'])

    return (time, ecg)